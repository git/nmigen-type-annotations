from typing import Optional, Generator, Any, Tuple, Iterable, Union
from .ast import Signal, Statement
from .cd import ClockDomain
# noinspection PyProtectedMember
from .dsl import FSM, Module
from abc import ABCMeta, abstractmethod

__all__ = ["Fragment", "Instance", "DriverConflict", "Elaboratable"]


class Elaboratable(metaclass=ABCMeta):
    @abstractmethod
    def elaborate(self, platform: Any) -> Module:
        ...


class DriverConflict(UserWarning):
    pass


# noinspection PyShadowingBuiltins
class Fragment:
    @staticmethod
    def get(obj: Any, platform: Any) -> 'Fragment':
        ...

    def add_ports(self, *ports: Any, dir: str) -> None:
        ...

    def iter_ports(self,
                   dir: Optional[str] = None) -> Generator[Signal, None, None]:
        ...

    def add_driver(self,
                   signal: Signal, domain: Optional[str] = None) -> None:
        ...

    def iter_drivers(self) -> Generator[Tuple[Optional[str], Signal],
                                        None, None]:
        ...

    def iter_comb(self) -> Generator[Signal, None, None]:
        ...

    def iter_sync(self) -> Generator[Tuple[str, Signal], None, None]:
        ...

    def iter_signals(self) -> Iterable[Signal]:
        ...

    def add_domains(self,
                    *domains: Union[Iterable[ClockDomain],
                                    ClockDomain]) -> None:
        ...

    def iter_domains(self) -> Generator[str, None, None]:
        ...

    def add_statements(self,
                       *stmts: Union[Iterable[Statement],
                                     Statement]) -> None:
        ...

    def add_subfragment(self,
                        subfragment: 'Fragment',
                        name: Optional[str] = None) -> None:
        ...

    def find_subfragment(self, name_or_index: Union[int, str]) -> 'Fragment':
        ...

    def find_generated(self, *path: Union[int, str]) -> FSM:
        ...

    def elaborate(self, platform: Any) -> 'Fragment':
        ...

    def prepare(self,
                ports: Iterable[Signal] = (),
                ensure_sync_exists: bool = True) -> 'Fragment':
        ...


class Instance(Fragment):
    # noinspection PyShadowingBuiltins
    def __init__(self, type: str, **kwargs: Any):
        ...
